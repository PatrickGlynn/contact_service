﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ContactService.DTO
{
    public class Person
    {
        public Person()
        {
            Addresses = new List<Address>();
            Relations = new List<RelationPerson>();
        }
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(100)]
        public string LastName { get; set; }
        [Required]
        [MaxLength(100)]
        [EmailAddress]
        public string EmailAddress { get; set; }
        [MaxLength(25)]
        public string HomePhoneNumber { get; set; }
        [MaxLength(25)]
        public string CellPhoneNumber { get; set; }
        [MaxLength(25)]
        public string WorkPhoneNumber { get; set; }
        public IList<Address> Addresses { get; set; }
        public IList<RelationPerson> Relations { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
        [MaxLength(50)]
        public string EmployeeId { get; set; }
        [DataType(DataType.Date)]
        public DateTime? DateOfHire { get; set; }
        public bool? CurrentlyEmployed { get; set; }
        public bool IsActive { get; set; }
    }
}
