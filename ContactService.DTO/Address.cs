﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ContactService.DTO
{
    public class Address
    {
        public int Id { get; set; }
        [Required]
        public int PersonId { get; set; }
        [Required]
        [MaxLength(50)]
        public string Type { get; set; }
        [Required]
        [MaxLength(150)]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        [Required]
        [MaxLength(100)]
        public string City { get; set; }
        [Required]
        [MaxLength(50)]
        public string State { get; set; }
        [Required]
        [MaxLength(20)]
        public string ZipCode { get; set; }
        public bool IsActive { get; set; }
    }
}
