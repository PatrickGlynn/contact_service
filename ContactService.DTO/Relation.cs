﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ContactService.DTO
{
    public class Relation
    {
        public int PersonId { get; set; }
        public int RelationPersonId { get; set; }
        public string RelationType { get; set; }
        public bool IsActive { get; set; }
    }
}
