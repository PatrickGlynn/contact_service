﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ContactService.DTO
{
    public class RelationPerson
    {
        public RelationPerson() 
        {
            Relation = new Relation();
            Person = new Person();
        }
        public Relation Relation { get; set; }
        public Person Person { get; set; }
    }
}
