﻿using ContactService.DTO;
using ContactService.Abstractions.Repository;
using System;
using Dapper;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;

namespace ContactService.Data
{
    public class PersonDataStore : IPersonDataStore
    {
        //NOTE: This is a locally provided MDF database, included in source control. There may be differences in how this DB gets connected to depending on the machine running it.
        //This implementation requires the installation of SQLEXPRESS to run properly.
        private static readonly string _connectionString = $@"Data Source=.\SQLEXPRESS;AttachDbFilename={AppDomain.CurrentDomain.BaseDirectory}Database\Contacts.mdf;Integrated Security=True;User Instance=True";

        public async Task<Person> CreatePersonAsync(Person person)
        {
            int personId = 0;
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"INSERT INTO Person(FirstName, LastName, EmailAddress, HomePhoneNumber, CellPhoneNumber, WorkPhoneNumber, DateOfBirth, DateOfHire, CurrentlyEmployed, EmployeeId)
                                VALUES (@FirstName, @LastName, @EmailAddress, @HomePhoneNumber, @CellPhoneNumber, @WorkPhoneNumber, @DateOfBirth, @DateOfHire, @CurrentlyEmployed, @EmployeeId);
                                SELECT CAST(SCOPE_IDENTITY() as int)";

                personId = await db.QuerySingleAsync<int>(sql, person);

                return await GetPersonByPersonIdAsync(personId);
            }
        }

        public async Task<Person> GetPersonByPersonIdAsync(int personId, bool activeOnly = true)
        {
            Person person = new Person();
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                //Get Person Info
                string sql = @"SELECT Id, FirstName, LastName, EmailAddress, HomePhoneNumber, CellPhoneNumber, WorkPhoneNumber, DateOfBirth, DateOfHire, CurrentlyEmployed, IsActive, EmployeeId
                                FROM Person
                                WHERE Id = @personId";

                if(activeOnly)
                    sql += " AND IsActive = 1";

                return await db.QueryFirstOrDefaultAsync<Person>(sql, new { personId });
            }
        }

        public async Task<Person> GetPersonByEmployeeIdAsync(string employeeId, bool activeOnly = true)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                //Get Person Info
                string sql = @"SELECT Id, FirstName, LastName, EmailAddress, HomePhoneNumber, CellPhoneNumber, WorkPhoneNumber, DateOfBirth, DateOfHire, CurrentlyEmployed, IsActive, EmployeeId
                                FROM Person
                                WHERE EmployeeId = @employeeId";

                if (activeOnly)
                    sql += " AND IsActive = 1";

                return await db.QueryFirstOrDefaultAsync<Person>(sql, new { employeeId });
            }
        }

        public async Task<IEnumerable<Person>> GetPersonByLastNameAsync(string lastName, bool activeOnly = true)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                //Get Person Info
                string sql = @"SELECT Id, FirstName, LastName, EmailAddress, HomePhoneNumber, CellPhoneNumber, WorkPhoneNumber, DateOfBirth, DateOfHire, CurrentlyEmployed, IsActive, EmployeeId
                                FROM Person
                                WHERE LastName = @lastName";

                if (activeOnly)
                    sql += " AND IsActive = 1";

                return await db.QueryAsync<Person>(sql, new { lastName });

            }
        }

        public async Task UpdatePersonAsync(Person person)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {

                string sql = @"UPDATE PERSON SET 
                                FirstName = @FirstName,
                                LastName = @LastName,
                                EmailAddress = @EmailAddress,
                                HomePhoneNumber = @HomePhoneNumber,
                                CellPhoneNumber = @CellPhoneNumber,
                                WorkPhoneNumber = @WorkPhoneNumber,
                                DateOfBirth = @DateOfBirth,
                                DateOfHire = @DateOfHire,
                                CurrentlyEmployed = @CurrentlyEmployed,
                                IsActive = @IsActive,
                                EmployeeId = @EmployeeId";

                await db.ExecuteAsync(sql, person);
            }
        }

        public async Task<Relation> CreateRelationAsync(Relation relation)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"INSERT INTO Relation(PersonId, RelationPersonId, RelationType)
                                VALUES(@PersonId, @RelationPersonId, @RelationType)";

                await db.ExecuteAsync(sql, relation);

                return await GetRelationByIdsAsync(relation.PersonId, relation.RelationPersonId);
            }
        }

        public async Task<Relation> GetRelationByIdsAsync(int personId, int relationPersonId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT PersonId, RelationPersonId, RelationType, IsActive
                                FROM Relation
                                WHERE PersonId = @personId AND RelationPersonId = @relationPersonId";

                return await db.QuerySingleAsync<Relation> (sql, new { personId, relationPersonId });
            }
        }

        public async Task<IEnumerable<Relation>> GetRelationsByPersonIdAsync(int personId, bool activeOnly = true)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                //Get Relation info
                string sql = @"SELECT PersonId, RelationPersonId, RelationType, IsActive
                                FROM Relation
                                WHERE PersonId = @personId";

                if (activeOnly)
                    sql += " AND IsActive = 1";

                return await db.QueryAsync<Relation>(sql, new { personId });
            }
        }

        public async Task UpdateRelationAsync(Relation relation)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"UPDATE Relation SET 
                                RelationType = @RelationType,
                                IsActive = @IsActive";

                await db.ExecuteAsync(sql, relation);
            }
        }

        public async Task<Address> CreateAddressAsync(Address address)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"INSERT INTO ADDRESS(Type, Address1, Address2, City, State, ZipCode, PersonId)
                                VALUES(@Type, @Address1, @Address2, @City, @State, @ZipCode, @PersonId)
                                SELECT CAST(SCOPE_IDENTITY() as int)";

                int addressId = await db.QuerySingleAsync<int>(sql, address);

                return await GetAddressByIdAsync(addressId);
            }
        }
        
        public async Task<Address> GetAddressByIdAsync(int addressId, bool activeOnly = true)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT Id, Type, Address1, Address2, City, State, ZipCode, PersonId, IsActive
                                FROM Address
                                WHERE Id = @addressId";

                if (activeOnly)
                    sql += " AND IsActive = 1";

                return await db.QuerySingleAsync<Address>(sql, new { addressId });

            }
        }
        public async Task<IEnumerable<Address>> GetAddressesByPersonIdAsync(int personId, bool activeOnly = true)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT Id, Type, Address1, Address2, City, State, ZipCode, PersonId, IsActive
                                FROM Address
                                WHERE PersonId = @personId";
                
                if (activeOnly)
                    sql += " AND IsActive = 1";

                return await db.QueryAsync<Address>(sql, new { personId });

            }
        }

        public async Task UpdateAddressAsync(Address address)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"UPDATE Address SET 
                                Type = @Type,
                                Address = @Address,
                                Address2 = @Address2,
                                City = @City,
                                State = @State,
                                ZipCode = @ZipCode,
                                IsActive = @IsActive";

                await db.ExecuteAsync(sql, address);
            }
        }
    }
}
