﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactService.Abstractions.Service;
using ContactService.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ContactService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ContactController : ControllerBase
    {
        private readonly IPersonService _personService;

        public ContactController(IPersonService personService)
        {
            _personService = personService;
        }

        [HttpGet]
        public async Task<ActionResult<IList<Person>>> Get([FromQuery(Name = "Id")]int? Id, [FromQuery(Name = "EmployeeId")] string employeeId, [FromQuery(Name = "LastName")]string lastName)
        {
            if(!Id.HasValue && String.IsNullOrEmpty(employeeId) && String.IsNullOrEmpty(lastName))
            {
                return NotFound();
            }

            IList<Person> p = new List<Person>();

            if (Id.HasValue)
            {
                Person person = await _personService.GetPersonByPersonIdAsync(Id.Value);
                if(person != null)
                    p.Add(person);
            }
            else if (!String.IsNullOrEmpty(employeeId))
            {
                Person person = await _personService.GetPersonByEmployeeIdAsync(employeeId);
                if(person != null)
                    p.Add(person);
            }
            else if (!String.IsNullOrEmpty(lastName))
            {
                p = (await _personService.GetPersonByLastNameAsync(lastName)).ToList();
            }

            if (!p.Any())
                return NotFound();

            foreach (Person person in p)
            {
                person.Addresses = (await _personService.GetAddressesByIdPersonIdAsync(person.Id)).ToList();

                IList<Relation> relations = (await _personService.GetRelationsByPersonIdAsync(person.Id)).ToList();

                foreach (Relation relation in relations)
                {
                    RelationPerson relationPerson = new RelationPerson();

                    relationPerson.Relation = relation;
                    relationPerson.Person = await _personService.GetPersonByPersonIdAsync(relation.RelationPersonId);

                    person.Relations.Add(relationPerson);
                }
            }

            return Ok(p);
        }

        [HttpPost]
        public async Task<ActionResult<Person>> Post([FromBody] Person person)
        {
            if (String.IsNullOrEmpty(person.HomePhoneNumber)
                    && String.IsNullOrEmpty(person.CellPhoneNumber)
                    && String.IsNullOrEmpty(person.WorkPhoneNumber))
            {
                ModelState.AddModelError("NoPhoneNumber", "You must provide at least one phone number");
            }


            if (ModelState.IsValid)
            {
                return await _personService.CreatePersonAsync(person);
            }

            return BadRequest(ModelState);
        }

        [HttpPut]
        public async Task<ActionResult<Person>> Put([FromBody]Person person)
        {
            //update person
            await _personService.UpdatePersonAsync(person);
            //update address
            foreach(Address address in person.Addresses)
                await _personService.UpdateAddressAsync(address);

            foreach(RelationPerson relationPerson in person.Relations)
            {
                //update relation
                await _personService.UpdateRelationAsync(relationPerson.Relation);
                //update relation person
                await _personService.UpdatePersonAsync(relationPerson.Person);

            }

            return await _personService.GetPersonByPersonIdAsync(person.Id);
        }

        [HttpDelete]
        [Route("{Id}")]
        public async Task<ActionResult> Delete(int personId)
        {
            Person p = await _personService.GetPersonByPersonIdAsync(personId);
            if (p == null)
                return NotFound();

            p.IsActive = false;
            await _personService.UpdatePersonAsync(p);

            return Ok();
        }
    }
}
