﻿using ContactService.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ContactService.Abstractions.Repository
{
    public interface IPersonDataStore
    {
        Task<Person> CreatePersonAsync(Person person);
        Task<Person> GetPersonByPersonIdAsync(int personId, bool activeOnly = true);
        Task<Person> GetPersonByEmployeeIdAsync(string employeeId, bool activeOnly = true);
        Task<IEnumerable<Person>> GetPersonByLastNameAsync(string lastName, bool activeOnly = true);
        Task UpdatePersonAsync(Person person);
        Task<Relation> CreateRelationAsync(Relation relation);
        Task<Relation> GetRelationByIdsAsync(int personId, int relationPersonId);
        Task<IEnumerable<Relation>> GetRelationsByPersonIdAsync(int personId, bool activeOnly = true);
        Task UpdateRelationAsync(Relation relationPerson);
        Task<Address> CreateAddressAsync(Address address);
        Task<Address> GetAddressByIdAsync(int addressId, bool activeOnly = true);
        Task<IEnumerable<Address>> GetAddressesByPersonIdAsync(int personId, bool activeOnly = true);
        Task UpdateAddressAsync(Address address); 
    }
}
