﻿using ContactService.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ContactService.Abstractions.Service
{
    public interface IPersonService
    {
        Task<Person> CreatePersonAsync(Person person);
        Task<Person> GetPersonByPersonIdAsync(int personId);
        Task<Person> GetPersonByEmployeeIdAsync(string employeeId);
        Task<IEnumerable<Person>> GetPersonByLastNameAsync(string lastName);
        Task UpdatePersonAsync(Person person);
        Task<Address> CreateAddressAsync(Address address, int personId);
        Task<IEnumerable<Address>> GetAddressesByIdPersonIdAsync(int personId);
        Task UpdateAddressAsync(Address address);
        Task<Relation> CreateRelationAsync(Relation relation);
        Task<IEnumerable<Relation>> GetRelationsByPersonIdAsync(int personId);
        Task UpdateRelationAsync(Relation relation);


    }
}
