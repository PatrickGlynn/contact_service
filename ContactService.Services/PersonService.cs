﻿using ContactService.Abstractions.Repository;
using ContactService.Abstractions.Service;
using ContactService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactService.Services
{
    public class PersonService : IPersonService
    {
        private readonly IPersonDataStore _contactDataStore;

        public PersonService(IPersonDataStore contactDataStore)
        {
            _contactDataStore = contactDataStore;
        }

        public async Task<Person> CreatePersonAsync(Person person)
        {
            Person p = await _contactDataStore.CreatePersonAsync(person);

            if(person.Addresses.Any())
            {
                foreach(Address address in person.Addresses)
                {
                    Address newAddress = await CreateAddressAsync(address, p.Id);

                    address.Id = newAddress.Id;
                }
            }

            if(person.Relations.Any())
            {
                foreach(RelationPerson relationPerson in person.Relations)
                {
                    //Create Person
                    relationPerson.Person = await CreatePersonAsync(relationPerson.Person);

                    Relation newRelation = new DTO.Relation()
                    {
                        PersonId = p.Id,
                        RelationPersonId = relationPerson.Person.Id,
                        RelationType = relationPerson.Relation.RelationType
                    };

                    relationPerson.Relation = await CreateRelationAsync(newRelation);
                }
            }

            return p;
        }

        public async Task<Person> GetPersonByPersonIdAsync(int personId)
        {
            return await _contactDataStore.GetPersonByPersonIdAsync(personId);
        }

        public async Task<Person> GetPersonByEmployeeIdAsync(string employeeId)
        {
            return await _contactDataStore.GetPersonByEmployeeIdAsync(employeeId);
        }

        public async Task<IEnumerable<Person>> GetPersonByLastNameAsync(string lastName)
        {
            return await _contactDataStore.GetPersonByLastNameAsync(lastName);
        }

        public async Task UpdatePersonAsync(Person person)
        {
            await _contactDataStore.UpdatePersonAsync(person);
        }

        public async Task<Address> CreateAddressAsync(Address address, int personId)
        {
            address.PersonId = personId;
            return await _contactDataStore.CreateAddressAsync(address);
        }

        public async Task<IEnumerable<Address>> GetAddressesByIdPersonIdAsync(int personId)
        {
            return await _contactDataStore.GetAddressesByPersonIdAsync(personId);
        }

        public async Task UpdateAddressAsync(Address address)
        {
            await _contactDataStore.UpdateAddressAsync(address);
        }

        public async Task<Relation> CreateRelationAsync(Relation relation)
        {
            return await _contactDataStore.CreateRelationAsync(relation);
        }

        public async Task<IEnumerable<Relation>> GetRelationsByPersonIdAsync(int personId)
        {
            return await _contactDataStore.GetRelationsByPersonIdAsync(personId);
        }

        public async Task UpdateRelationAsync(Relation relation)
        {
            await _contactDataStore.UpdateRelationAsync(relation);
        }
    }
}
